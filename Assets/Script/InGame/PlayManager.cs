using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using TMPro;
using UnityEngine.Events;

public class PlayManager : MonoBehaviour
{
    [SerializeField] BallController ballController;
    [SerializeField] CameraController camController;
    [SerializeField] GameObject finishWindow;
    [SerializeField] TMP_Text finishText;
    [SerializeField] TMP_Text shootCountText;

    public UnityEvent OnPause;
    bool isBallOutside;
    bool isBallTeleporting;
    bool isGoal;
    bool isPaused = false;
    Vector3 lastBallPosition;
    private void Update()
    {
        if(ballController.ShootingMode)
        {
            lastBallPosition = ballController.transform.position;
        }
        var inputActive = Input.GetMouseButton(1) 
            && ballController.ShootingMode == false
            && isBallOutside == false; 

        camController.SetInputActive(inputActive);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isGoal)
            {
                return;
            }

            if (isPaused)
            {
                return;
            }

            OnPause.Invoke();
        }
    }

    private void OnEnable()
    {
        ballController.onBallShooted.AddListener(UpdateShootCount);
    }

    private void OnDisable()
    {
        ballController.onBallShooted.RemoveListener(UpdateShootCount);
    }

    public void PauseGame()
    {
        // check if the game is paused
        if (isPaused == true)
        {
            Time.timeScale = 1;
            isPaused = false;
        }
        else
        {
            Time.timeScale = 0;
            isPaused = true;
        }
    }

    public void OnBallGoalEnter()
    {
        isGoal = true;
        ballController.enabled = false;
        //TODO player window win popup
        finishWindow.gameObject.SetActive(true);
        finishText.text = "HOLE IN!!! \n" + "Shoot Count : " + ballController.ShootCount;
    }

    public void OnBallOutside()
    {
        if (isGoal)
        {
            return;
        }
        isBallOutside = true;
        if (isBallTeleporting == false)
        {
            Invoke("TeleportBallLastPosition", 0.5f);
        }

        ballController.enabled = false;
        isBallOutside = true;
        isBallTeleporting = true;
    }

    public void TeleportBallLastPosition()
    {
        TeleportBall(lastBallPosition);
    }
    public void TeleportBall(Vector3 targetPosition)
    {
        var rb = ballController.GetComponent<Rigidbody>();
        rb.isKinematic = true;
        ballController.transform.position = lastBallPosition;
        rb.isKinematic = false;

        ballController.enabled = true;
        isBallOutside = false;
        isBallTeleporting = false;
    }

    public void UpdateShootCount(int shootCount)
    {
        shootCountText.text = shootCount.ToString();
    }
}
